import pygame
from jeu import Jeu


def route():
    if __name__ == '__main__':
        pygame.init()
        j = Jeu()
        j.go()


pygame.init()
ecran = pygame.display.set_mode((1200, 848))
pygame.display.set_caption("Kittycat")
background = pygame.image.load("D:\\cours\\NSI\\Term\\trophé\\image\\Fond entre.jpg")
background.convert()


# commande de mis en route
def touche_cl1():
    touche1 = pygame.key.get_pressed()
    if touche1[pygame.K_SPACE] or touche1[pygame.K_RETURN]:
        route()


run = True
while run:
    touche_cl1()
    ecran.blit(background, (0, 0))
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
pygame.quit()
