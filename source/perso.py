import pygame
import random
from moviepy.editor import VideoFileClip


class Entiter(pygame.sprite.Sprite):
    def __init__(self,  x, y):
        super().__init__()
        self.sprite_sheet = pygame.image.load("D:\\cours\\NSI\\Term\\trophé\\image\\perso\\Kitty cat.png")
        self.image = self.get_image(0, 0)
        self.image.set_colorkey([0, 0, 0])
        self.rect = self.image.get_rect()
        self.position = [x, y]
        self.speed = 3
        self.pied = pygame.Rect(0, 0, 40, 80)
        self.position2 = self.position.copy()
        self.bisscuit = 0
        self.vie = 700

    def perd_vie(self):
        self.vie -= 1

    def gain(self):
        g = random.randint(5, 10)
        self.bisscuit += g
        return self.bisscuit

    def enregistrer_loca(self):
        self.position2 = self.position.copy()

    def depl_d(self):
        self.position[0] += self.speed

    def depl_g(self):
        self.position[0] -= self.speed

    def saut(self):
        self.position[1] -= self.speed

    def depl_b(self):
        self.position[1] += 6

    def update(self):
        self.rect.topleft = self.position
        self.pied.midbottom = self.rect.midbottom

    def annule_depl(self):
        self.position = self.position2
        self.rect.topleft = self.position
        self.pied.midbottom = self.rect.midbottom

    def mystere(self):
        r = random.random()
        if r <= 0.3 and self.speed < 15:
            self.speed += 5
        elif 0.3 < r <= 6:
            self.gain()
        else:
            self.vie += 100

    def get_image(self, x, y):
        image = pygame.Surface([40, 80])
        image.blit(self.sprite_sheet, (0, 0), (x, y, 40, 80))
        return image

    def image_fin(self):
        pygame.init()
        clip = VideoFileClip("D:\\cours\\NSI\\Term\\trophé\\image\\perso\\kittycat-danse.mp4")

        def getSurface(t, srf=None):
            frame = clip.get_frame(t=t)
            if srf is None:
                return pygame.surfarray.make_surface(frame.swapaxes(0, 1))
            else:
                pygame.surfarray.blit_array(srf, frame.swapaxes(0, 1))
                return srf
        surface = getSurface(0)
        screen = pygame.display.set_mode(surface.get_size(), 0, 32)
        running = True
        t = 0
        while running:
            screen.blit(getSurface(t, surface), (0, 0))
            font = pygame.font.SysFont('freesansbold.ttf', 70)
            text = font.render("Félicitation vous avez gagné !!!", True, [240, 115, 105])
            textRect = text.get_rect()
            textRect.center = (450, 700)
            screen.blit(text, textRect)
            pygame.display.flip()
            t += 1/400
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    quit()


