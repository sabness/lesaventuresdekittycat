import pygame
import pytmx
import pyscroll
from moviepy.editor import VideoFileClip
from perso import Entiter


class Jeu:
    def __init__(self):
        global text
        global textRect
        # fenetre
        self.fenetre = pygame.display.set_mode((1200, 600))
        pygame.display.set_caption("Kittycat")

        # carte
        tmx_data = pytmx.util_pygame.load_pygame("D:\\cours\\NSI\\Term\\trophé\\carte.tmx")
        map_data = pyscroll.data.TiledMapData(tmx_data)
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.fenetre.get_size())

        # joueur
        position_perso = tmx_data.get_object_by_name("personage")
        self.joueur = Entiter(position_perso.x, position_perso.y)


        # colision
        self.block = []
        self.mis = []
        self.tresore = []
        self.colision_b = []
        for obj in tmx_data.objects:
            if obj.type == "colision":
                self.block.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))
            elif obj.type == "mis":
                self.mis.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))
            elif obj.type == "tresore":
                self.tresore.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))
            elif obj.type == "bombe_1" or obj.type == "bombe_2" or obj.type == "bombe_3" or obj.type == "bombe_4" or obj.type == "bombe_5" or obj.type == "bombe_6" or obj.type == "bombe_7" or obj.type == "bombe_8" or obj.type == "bombe_9" or obj.type == "bombe_10" or obj.type == "bombe_11":
                self.colision_b.append(pygame.Rect(obj.x, obj.y, obj.width, obj.height))

        # calques
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=1)
        self.group.add(self.joueur)

    def get_image(self, x, y):
        image = pygame.Surface([40, 80])
        image.blit(self.sprite_sheet, (0, 0), (x, y, 50, 43))
        return image

    def affiche_texte(self):
        font = pygame.font.Font('freesansbold.ttf', 30)
        text = font.render(str(self.joueur.bisscuit) + " Kitty krugs", True, [0, 0, 255])
        textRect = text.get_rect()
        textRect.center = (1080, 30)
        self.fenetre.blit(text, textRect)
        texte = font.render(str(self.joueur.vie)[0]+" Vies", True, [0, 0, 255])
        texteRect = texte.get_rect()
        texteRect.center = (55, 30)
        self.fenetre.blit(texte, texteRect)
        textv = font.render(str(self.joueur.speed)+" m/s", True,[0, 0, 255])
        textvRect = textv.get_rect()
        textvRect.center = (600, 30)
        self.fenetre.blit(textv, textvRect)

    def image_perdue(self):
        pygame.init()
        clip = VideoFileClip("D:\\cours\\NSI\\Term\\trophé\\image\\perso\\perdue.MP4")
        def getSurface(t, srf=None):
            frame = clip.get_frame(t=t)
            if srf is None:
                return pygame.surfarray.make_surface(frame.swapaxes(0, 1))
            else:
                pygame.surfarray.blit_array(srf, frame.swapaxes(0, 1))
                return srf
        surface = getSurface(0)
        screen = pygame.display.set_mode(surface.get_size(), 0, 32)
        running = True
        t = 0
        while running:
            screen.blit(getSurface(t, surface), (0, 0))
            font = pygame.font.SysFont('freesansbold.ttf', 60)
            text = font.render("Vous avez perdu, Kitty cat est mort", True, [128, 252, 74])
            textRect = text.get_rect()
            textRect.center = (425, 700)
            screen.blit(text, textRect)
            pygame.display.flip()
            t += 1 / 400
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    quit()




    def update(self):
        self.group.update()
        self.affiche_texte()
        for sprite in self.group.sprites():
            if sprite.pied.collidelist(self.block) > -1:
                sprite.annule_depl()
            if sprite.pied.collidelist(self.mis) > -1:
                self.mis.pop(0)
                sprite.annule_depl()
                sprite.mystere()
            if sprite.pied.collidelist(self.tresore) > -1:
                sprite.annule_depl()
                sprite.image_fin()
            if sprite.pied.collidelist(self.colision_b) > -1:
                if self.joueur.vie > 100:
                    sprite.perd_vie()
                else:
                    self.image_perdue()

    # commande de deplacement
    def touche_cl(self):
        touche = pygame.key.get_pressed()
        if touche[pygame.K_UP] or touche[pygame.K_z] or touche[pygame.K_SPACE]:
            self.joueur.saut()
        elif touche[pygame.K_LEFT] or touche[pygame.K_q]:
            self.joueur.depl_g()
        elif touche[pygame.K_RIGHT] or touche[pygame.K_d]:
            self.joueur.depl_d()
        elif True:
            self.joueur.depl_b()

    def go(self):
        clock = pygame.time.Clock()
        fo = True
        while fo:
            self.joueur.enregistrer_loca()
            self.touche_cl()
            self.group.center(self.joueur.rect)
            self.group.draw(self.fenetre)
            self.update()
            pygame.display.flip()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    fo = False
            clock.tick(60)
        pygame.quit()
        quit()
